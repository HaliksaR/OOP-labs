/*
Задание 1 Написать процедуры и функции для работы со списком
	A. добавление элемента после к-ого элемента списка
	B. удаление из списка к-ого элемента
	C. подсчет числа элементов в списке
	D. перемещение р-ого элемента списка после к-ого элемента
	С. помощью этих процедур и функций создать список следующим образом. Включать в список полные квадраты из одномерного массива целых чисел (типа Byte). Удалить повторяющиеся элементы списка. Определить длину полученного списка.
https://tproger.ru/translations/stacks-and-queues-for-beginners/
Задание 2 Написать программу, которая визуально демонстрирует работу стека и очереди. Длина стека и очереди ограничена 10 элементами.
*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include "stack.h"
#include "queue.h"
#include "header.h"

#define n 10
using namespace std;

int printmenu() {
    wcout << L"\n -<Меню>-\n 0 Выход" << endl;
    wcout << L"Стек" << endl;
    wcout << L" 1 Добавить" << endl;
    wcout << L" 2 Удалить последний добавленный" << endl;
    wcout << L" 3 Удалить все" << endl;
    wcout << L" 4 Напечатать стэк" << endl;
    wcout << L"Очередь" << endl;
    wcout << L" 5 Добавить" << endl;
    wcout << L" 6 Удалить" << endl;
    wcout << L" 7 Определить первый элемент" << endl;
    wcout << L" 8 Напечатать очередь" << endl;
    wcout << L" ВЫБОР: ";
    return 0;
}

int full_square(int x) {
    double num = x;
    if (num > 1 && sqrt(num) - static_cast<int> (sqrt(num)) == 0) {
        return 0;
    }
    return -1;
}

int main() {
    setlocale(LC_ALL, "");
    int answ;
    wcout << L"1 список 2 остальное :";
    cin >> answ;
    if (answ == 2) {
        stack *MyStack = new stack;
        queue q1;
        createstack(*MyStack);
        int req = -1;
        while (req != 0) {
            int r;
            printmenu();
            cin >> req;
            switch (req) {
                case 1:
                    wcout << L"Вставка элемента в стек: ";
                    cin >> r;
                    if (full_square(r) == -1) {
                        wcout << L"Не квадратное число!" << endl;
                        break;
                    }
                    if (push(*MyStack, r) == -1)
                        wcout << L"\tПереполнение!" << endl;
                    if (push(*MyStack, r) == -2) {
                        wcout << L"Такой элемент уже есть!" << endl;
                    }
                    printstack(*MyStack);
                    break;
                case 2:
                    pop(*MyStack, lenstack(*MyStack));
                    printstack(*MyStack);
                    break;
                case 3:
                    sdel(*MyStack);
                    printstack(*MyStack);
                    break;
                case 4:
                    printstack(*MyStack);
                    break;
                case 5:
                    wcout << L"Вставка элемента в очередь: ";
                    cin >> r;
                    if (full_square(r) == -1) {
                        wcout << L"Не квадратное число!" << endl;
                        break;
                    }
                    if (q1.isqueuefull() == 0) {
                        if (q1.ins(r) != 0) {
                            wcout << L"Такой элемент уже есть!" << endl;
                        }
                    } else
                        wcout << L"Очередь переполнена" << endl;
                    if (q1.isqueueempty() == 0) {
                        q1.show_queue();
                    } else {
                        wcout << L"Очередь пуста" << endl;
                    }
                    break;
                case 6:
                    if (q1.isqueueempty() == 0) {
                        int ele = q1.del();
                        wcout << L"Удален элемент:" << ele << endl;
                    } else {
                        wcout << L"Очередь пуста" << endl;
                    }
                    if (q1.isqueueempty() == 0) {
                        q1.show_queue();
                    } else {
                        wcout << L"Очередь пуста" << endl;
                    }
                    break;
                case 7:
                    if (q1.isqueueempty() == 0) {
                        int ele = q1.peek();
                        wcout << L"Первый элемент элемент:" << ele << endl;
                    } else {
                        wcout << L"Очередь пуста" << endl;
                    }
                    break;
                case 8:
                    if (q1.isqueueempty() == 0) {
                        q1.show_queue();
                    } else {
                        wcout << L"Очередь пуста" << endl;
                    }
                    break;
            }
        }
    } else {
        int *arr = new int[n];
        int i = 0, score = 0, side = 0;

        for (i = 0; i < n; i++) {
            arr[i] = 1 + rand() % 10000;
        }
        // гарантируем наличие полных квадратов
        arr[2] = 1;
        arr[5] = 25;
        arr[7] = 49;

        wcout << L"\033[31m Массив такой получается...\n\n";
        for (i = 0; i < n; i++) {
            if (quadro(arr[i])) {
                wcout << L"\033[35m " << arr[i] <<" \033[0m";
            } else {
                wcout << L"\033[36m" << arr[i] <<  "\033[0m";
            }
        }
        wcout << L"\n";

        // начинаем процедуру добавления
        ptrlist ptrnode = NULL; // указатель на добавляемый узел списка
        ptrlist pointer = NULL; // указатель на 1 (0) узел списка

        for (i = 0; i < n; i++) {
            if (quadro(arr[i])) {
                ptrnode = create_node(arr[i]);
                if (pointer == NULL) {
                    pointer = ptrnode;
                    wcout << L"\nВо главе списка сохранено чило\n" << pointer->num;
                    score = concider(pointer);
                    print_list(pointer, score);
                    ptrnode = NULL;
                } else {
                    wcout << L"\nДобавляем \n" << ptrnode->num;
                    side = search(pointer, ptrnode);    // находим позицию ПОСЛЕ КОТОРОЙ нужно вставить
                    wcout << L"Вставить элемент нужно после" << side << "(-го) элемента\n";
                    wcout << L"Вставляем элемент в список\n";
                    pointer = real_add(pointer, ptrnode, side);
                    score = concider(pointer);
                    print_list(pointer, score);
                }
            }
        }

        wcout << L"\nЧистка...\n";
        del_repetition(pointer);
        score = concider(pointer);
        print_list(pointer, score);
    }
    return 0;
}


int quadro(int a) {
    int b = sqrt(a);
    if (pow(b, 2) == a) {
        // полный кадрат
        return 1;
    } else {
        return 0;
    }
}

ptrlist create_node(int a) {
    struct list *l = new struct list[1];
    l->num = a;
    l->pb = NULL;
    l->pn = NULL;

    return l;
}

int search(ptrlist a, ptrlist b) {
    // функция определяет позицию, после которой нужно вставить элемент
    // a - список, b - добавляемый элемент
    int side = -1; // раньше, чем 0 элемент списка
    ptrlist next = NULL, t = NULL;
    if (a->num < b->num) {
        // новый элемент самый большой
        return side;
    } else {
        side = 0;
        next = a->pn;
    }

    while (1) {
        if (next == NULL || (next->num < b->num)) {
            return side;
        } else {
            side++;
            t = next;
            next = t->pn;
        }
    }
}

ptrlist real_add(ptrlist a, ptrlist b, int side) {
    // встраивает элемент b после элемента под порядковым номером side
    ptrlist local = a, next = a->pn;
    int i = 0;
    if (side == -1) {
        // встроить необходимо во главу списка
        b->pn = local;
        local->pb = b;
        local = b;
        return local;
    }

    while (1) {
        if (i == side) {
            b->pn = next;
            b->pb = local;
            next->pb = b;
            local->pn = b;
            return a;
        } else {
            i++;
            local = next;
            next = local->pn;
        }
    }
}

int concider(ptrlist a) {
    int k = 0;
    ptrlist note, local = a;

    while (local != NULL) {
        k++;
        note = local;
        local = note->pn;
    }

    return k;
}

void print_list(ptrlist a, int b) {
    int i = 0;
    ptrlist local = a;

    wcout << L"\n\033[32mСписок содержит " << b <<" элементов(-a):\n\n";
    while (local != NULL) {
        wcout << L"\033[32m " << local->num << "\033[0m\t";
        local = local->pn;
    }
    wcout << L"\n";
}

void del_repetition(ptrlist a) {
    ptrlist local = a, t, dop;
    int buf = local->num; // буфер для хранения последнего значения
    local = local->pn;
    // предполается, что список отсортирован по убыванию
    while (local != NULL) {
        if (local->num == buf) {
            t = local->pn;
            dop = local->pb;
            dop->pn = t;
            t->pb = dop;
            free(local);
            local = t;
            t = NULL;
            dop = NULL;
        } else {
            buf = local->num;
            local = local->pn;
        }
    }
}


void swap(ptrlist a, ptrlist b) {
    int t;
    t = a->num;
    a->num = b->num;
    b->num = t;
}