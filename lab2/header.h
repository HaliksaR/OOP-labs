#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>

struct list {
    struct list* pb;// указатель на предыдущий лист
    struct list* pn;// указатель на следующий лист
    int num;		// число списка
};

typedef struct list* ptrlist;

int quadro(int);
ptrlist create_node(int);
int search(ptrlist, ptrlist);
void del_repetition(ptrlist);
void print_list(ptrlist, int);
int concider(ptrlist);
ptrlist real_add(ptrlist, ptrlist, int);
void swap(ptrlist a, ptrlist b);