#define SIZE 10

#include "stack.h"

class queue {
public:
    int array[SIZE];
    int f, r;
    queue() {
        f = 0;
        r = -1;
    }

    int ins(int x) {
        for (int i = 0; i < r; i++) {
            if (array[i] == x) {
                return -1;
            }
        }
        r++;
        array[r] = x;
        return 0;
    }

    int del() {
        int x;
        x = array[f];
        f++;
        return x;
    }

    int peek() {
        int x;
        x = array[f];
        return x;
    }

    void show_queue() {
        int i;
        wcout << endl;
        for (i = f; i <= r; i++) {
            wcout << L"<" << array[i];
        }
        wcout << L"<<<" << r + 1 << L">>>" << endl;
    }

    int isqueueempty() {
        if (f == (r + 1))
            return 1;
        else
            return 0;
    }

    int isqueuefull() {
        if (r == SIZE - 1)
            return 1;
        else
            return 0;
    }
};
