/*
Задание 1. Создать динамический двумерный массив размером 200x200 элементов, заполнить его случайными целыми числами (типа Byte). Переписать элементы матрицы во вновь созданный одномерный динамический массив в следующем порядке
	a) по правым диагоналям, начиная с правого верхнего элемента
	b) по левым диагоналям, начиная с левого верхнего элемента
	c) по спирали, начиная с центрального элемента
	d) по спирали, начиная с левого верхнего элемента
Задание 2. Создать двумерный динамический массив с произвольным количеством элементов в каждой сроке. Заполнить его и распечатать построчно. 
*/
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

int main() {
    setlocale(LC_ALL, "");

    int size = 10;
    int i, j;

    auto **Array = new int *[size];
    for (i = 0; i < size; i++) {
        Array[i] = new int[size];
        for (j = 0; j < size; j++) {
            Array[i][j] = i * size + j;
            cout << setw(3) << Array[i][j];
        }
        cout << endl;
    }
    auto *Array_2 = new int[size * size];
    for (i = 0; i < size * size; i++) {
        Array_2[i] = 0;
    }

    wcout << L"a) по правым диагоналям, начиная с правого верхнего элемента" << endl;
    int x, y, z = 0, k = 0;
    for (i = size - 1; i >= 0; i--, z = 0) {
        Array_2[k] = Array[z][i];
        k++;
        for (j = i + 1; j < size; j++) {
            z++;
            Array_2[k] = Array[z][j];
            k++;
        }
    }
    for (i = 1; i <= size - 1; i++, z = 0) {
        Array_2[k] = Array[i][z];
        k++;
        for (j = i + 1; j < size; j++) {
            z++;
            Array_2[k] = Array[j][z];
            k++;
        }
    }

    for (i = 0; i < size * size; i++) {
        cout << setw(3) << Array_2[i];
    }
    cout << endl;

    wcout << L"b) по левым диагоналям, начиная с левого верхнего элемента" << endl;
    k = 0;
    for (i = 0; i <= size - 1; i++, z = 0) {
        Array_2[k] = Array[z][i];
        k++;
        for (j = size - i; j < size; j++) {
            z++;
            Array_2[k] = Array[z][size - j - 1];
            k++;
        }
    }
    for (i = 1; i <= size - 1; i++) {
        z = size - 1;
        Array_2[k] = Array[i][z];
        k++;
        for (j = i + 1; j < size; j++) {
            z--;
            Array_2[k] = Array[j][z];
            k++;
        }
    }
    for (i = 0; i < size * size; i++) {
        cout << setw(3) << Array_2[i];
    }
    cout << endl;

    wcout << L"c) по спирали, начиная с центрального элемента" << endl;
    x = (size - 1) / 2;
    y = x;
    int lim = 0, count = 0;
    while (lim <= size) {
        lim++;
        for (i = 0; (i < lim) && (y < size) && (y >= 0) && (x < size) && (x >= 0); i++) {//вправо
            Array_2[count++] = Array[x][y++];
        }
        for (i = 0; (i < lim) && (y < size) && (y >= 0) && (x < size) && (x >= 0); i++) {//вниз
            Array_2[count++] = Array[x++][y];
        }
        lim++;
        for (i = 0; (i < lim) && (y < size) && (y >= 0) && (x < size) && (x >= 0); i++) {//влево
            Array_2[count++] = Array[x][y--];
        }
        for (i = 0; (i < lim) && (y < size) && (y >= 0) && (x < size) && (x >= 0); i++) {//вверх
            Array_2[count++] = Array[x--][y];
        }
    }

    for (i = 0; i < size * size; i++) {
        cout << setw(3) << Array_2[i];
    }
    cout << endl;

    wcout << L"d) по спирали, начиная с левого верхнего элемента" << endl;
    lim = size / 2;
    count = 0;
    int tmp;
    for (tmp = 0; tmp < lim; tmp++) {
        for (i = tmp; i < size - tmp; i++, count++) {//колонка вниз
            Array_2[count] = Array[i][tmp];
        }
        for (j = tmp + 1; j < size - tmp; j++, count++) {//строка вправо
            Array_2[count] = Array[i - 1][j];
        }
        for (i = size - tmp - 2; i >= tmp; i--, count++) {//колонка вверх
            Array_2[count] = Array[i][j - 1];
        }
        for (j = size - tmp - 2; j >= tmp + 1; j--, count++) {//строка влево
            Array_2[count] = Array[i + 1][j];
        }
    }

    if (size % 2 != 0) {
        Array_2[count] = Array[lim][lim];
    }
    for (i = 0; i < size * size; i++) {
        cout << setw(3) << Array_2[i];
    }
    cout << endl;

    for (i = 0; i < size; i++) {
        delete[]Array[i];
    }
    delete[]Array_2;

    wcout << L"Задание 2" << endl;
    int size1 = 10, size2;
    auto **Array2 = new int *[size1];
    for (i = 0; i < size1; i++) {
        size2 = rand() % 10 + 3;
        Array2[i] = new int[size2];
        for (j = 0; j < size2; j++) {
            Array2[i][j] = i * size1 + j;
            cout << setw(4) << Array2[i][j];
        }
        cout << endl;
    }

    for (i = 0; i < size1; i++) {
        delete[]Array2[i];
    }

    return 0;
}