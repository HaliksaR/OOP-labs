#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include "includes/Product.h"
#include "includes/Map.h"
#include "includes/TerminalPublic.h"
#include "includes/TerminalRoot.h"
#include "includes/color.h"
#include "includes/generate_id.h"

using namespace std;

ifstream file;

Map<string, Product> parsingData(TerminalPublic &visual) {
    string token, property, value;
    int items = -1;
    while (!file.eof()) {
        getline(file, token, '{');
        getline(file, token, '}');
        items++;
    }
    file.clear();
    file.seekg(0);
    Product product[items];
    Map<string, Product> data;
    for (int size = 0; !file.eof(); size++) {
        product[size].setStatus(1);
        getline(file, token, ':');
        if (token.length() <= 0) {
            break;
        }
        product[size].setName(token);
        getline(file, token, '=');

        if (token == "quantity") {
            getline(file, token, ':');
            product[size].setQuantity(stoi(token));
        } else {
            file.close();
            cout << "ERROR: data incorrectly filled!" << token << endl;
            exit(-2);
        }

        getline(file, token, '=');
        if (token == "price") {
            getline(file, token, '{');
            product[size].setPrice(stof(token));
        } else {
            file.close();
            cout << "ERROR: data incorrectly filled!:" << token << endl;
            exit(-2);
        }
        getline(file, token, '}');
        int count = -1;
        for (int i = 0; i < token.length(); i++) {
            if (token[i] == '\n') {
                count++;
            }

        }
        istringstream iterToken(token);
        for (int i = 0; i < count; i++) {
            getline(iterToken, token, '\n');
            getline(iterToken, token, ':');
            property = token;
            product[size].propertiesArray.push_back(property);
            getline(iterToken, token, ';');
            value = token;
            product[size].setProperties(property, value);
        }
        iterToken.clear();
        visual.arrayId.push_back(generateId(size, product[size].getName()));
        product[size].setId(visual.arrayId[size]);
        data.add(visual.arrayId[size], product[size]);
        getline(file, token, '\n');
    }
    file.close();
    return data;
};

Map<string, Product> readData(TerminalPublic &visual) {
    file.open("../data/Product.txt");
    Map<string, Product> data;
    if (!file) {
        cerr << "ERROR: DATA FILE NOT FOUND" << endl;
        exit(-1);
    }
    data = parsingData(visual);
    return data;
}

int eventsToKey(string events) {
    if (events == "add") return 0;
    if (events == "delete") return 1;
    if (events.length() == 0) return 4;
    return 3;
}

Map<string, Product> Root_terminal(Map<string, Product> &Products, TerminalRoot RootVisual) {
    string events;
    while (eventsToKey(events) != 3) {
        cout << "add || delete || exit" << endl;
        cin >> events;
        switch (eventsToKey(events)) {
            case 0: {
                if (RootVisual.addProduct(Products)) {
                    COUTGREEN << "Add done!" << ENDLRESET;
                } else {
                    COUTRED << "ADD ERROR!" << ENDLRESET;
                }
                break;
            }
            case 1: {
                string str = "show";
                while (str == "show" || str == "id") {
                    cout << "Please write id for remove (show id(id), property(show)):";
                    cin >> str;
                    Terminal show;
                    if (str == "show") {
                        show.print(Products, RootVisual.arrayId);
                        continue;
                    }
                    if (str == "id") {
                        show.printId(Products, RootVisual.arrayId);
                        continue;
                    }
                    if (RootVisual.removeProduct(Products, str)) {
                        COUTGREEN << "Delete: " + str << ENDLRESET;
                        continue;
                    } else {
                        COUTRED << "DELETE ERROR: " + str << ENDLRESET;
                        continue;
                    }
                }
                break;
            }
        }
    }
    return Products;
}

bool saveData(Map<string, Product> Products, Terminal visual) {
    ofstream firstfile;
    firstfile.open("../data/Product.txt");
    if (!firstfile.is_open()) {
        return false;
    }
    for (int j = 0; j < visual.arrayId.size(); j++) {
        firstfile << rendering(Products, visual.arrayId[j]);
    }
    return true;
}

bool RootLogin(string login, string pass, TerminalRoot RootVisual) {
    return RootVisual.validation(login + "&" + pass);
}

int main() {
    TerminalPublic visual;
    TerminalRoot RootVisual;
    Map<string/* NameStack*/, Product/*Products*/> Products = readData(visual);
    Map<string, int> Names;
    bool done = false;
//    visual.print(Products, visual.arrayId);
    while (!done) {
        visual.printMenu();
        RootVisual.arrayId = visual.arrayId;
        int answi;
        cin >> answi;
        if (answi > 6 && answi < 0) {
            return -1;
        }
        switch (answi) {
            case 0: {
                cout << "Search id by tag ( ";
                Names.clear();
                for (int i = 0; i < Products.lenght(); i++) {
                    Names.add(Products.operator[](visual.arrayId[i]).getName(), 0);
                }
                Names.printIndex();
                cout << "):";
                string answ;
                cin >> answ;
                visual.searchProduct(Products, answ, visual.arrayId);
                break;
            }
            case 1: {
                cout << "Get info by tag or id:";
                string answ;
                cin >> answ;
                visual.GetInfoProduct(Products, answ, visual.arrayId);
                break;
            }
            case 2: {
                cout << "Buy by tag or id and quantity:";
                string answ;
                cin >> answ;
                int qua;
                cin >> qua;
                visual.buyProduct(Products, answ, qua, visual.arrayId);
                break;
            }
            case 3: {
                cout << "Search by Properties:" << endl;
                cout << "Print Name:" << endl;
                string name, value;
                cin >> name;
                cout << "Print Properties value:" << endl;
                cin >> value;
                visual.searchPropertiesProduct(Products, name, value, visual.arrayId);
                break;
            }
            case 4: {
                string login, pass;
                cout << "Login:";
                cin >> login;
                cout << "Password:";
                cin >> pass;
                if (RootLogin(login, pass, RootVisual)) {
                    Products = Root_terminal(Products, RootVisual);
                    visual.arrayId = RootVisual.arrayId;
                } else {
                    COUTRED << "Incorrect input!" << ENDLRESET;
                }
                break;
            }
            case 5:
                if (saveData(Products, visual)) {
                    COUTGREEN << "Goodbye!" << ENDLRESET;
                    return 0;
                } else {
                    COUTRED << "EXIT ERROR!" << ENDLRESET;
                    exit(-1);
                }
            default:
                break;
        }
    }
    return 0;
}