#ifndef LAB7_TERMINALPUBLIC_H
#define LAB7_TERMINALPUBLIC_H


#include "Product.h"
#include "Map.h"
#include "Terminal.h"
#include <vector>
#include <string>

class TerminalPublic: public Terminal {

public:
    TerminalPublic();
    ~TerminalPublic();

    vector<string>  searchPropertiesProduct(Map<string, Product>, string, string, vector<string>);
    std::vector<std::string> searchProduct(Map<std::string,Product>, std::string, std::vector<std::string>);
    Map<string, Product> buyProduct(Map<std::string,Product>&, std::string, int, std::vector<std::string>&);
    bool GetInfoProduct(Map<std::string,Product>&, std::string, std::vector<std::string>);
};

#endif //LAB7_TERMINALPUBLIC_H
