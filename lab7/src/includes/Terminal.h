#ifndef LAB7_TERMINAL_H
#define LAB7_TERMINAL_H

#include <vector>
#include <string>
#include "Map.h"
#include "Product.h"

class  Terminal {

public:
    Terminal();
    ~Terminal();

    std::vector<std::string> arrayId;

    void printMenu();
    void print(Map<std::string,Product>&, std::vector<std::string>);
    void printId(Map<std::string, Product>&, std::vector<std::string>);
    void printProperties(Map<std::string, Product>&, std::string);
    void printTitle(Map<std::string, Product>&, std::string);
};

#endif //LAB7_TERMINAL_H
