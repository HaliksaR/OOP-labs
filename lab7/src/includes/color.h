#ifndef LAB7_COLOR_H
#define LAB7_COLOR_H
//http://www.xserver.ru/computer/os/linux/99/
#define RESET "\033[0m"
#define GREEN "\033[1;32m"
#define RED   "\033[1;31m"

#define COUTRED cout << RED
#define COUTGREEN cout << GREEN
#define ENDLRESET  RESET<<endl

#endif //LAB7_COLOR_H
