#ifndef LAB7_MAP_H
#define LAB7_MAP_H

#include <vector>
#include <iostream>

using namespace std;

template<class KEY, class VALUE>
class Map {
private:
    vector<pair<KEY, VALUE> > cont;

    bool exists(const KEY &key) {
        for (typename vector<pair<KEY, VALUE> >::iterator it = cont.begin(); it != cont.end(); ++it)
            if (it->first == key) return true;
        return false;
    }

public:
    bool add(const KEY &key, const VALUE &value) {
        if (exists(key)) return false;
        cont.push_back(make_pair(key, value));
        return true;
    }

    bool remove(const KEY &key, const VALUE &value) {
        int index = 0;
        for (typename vector<pair<KEY, VALUE> >::iterator it = cont.begin(); it != cont.end(); ++it) {
            if (it->first == key) {
                cont.erase(cont.begin() + index);
                return true;
            }
            index++;
        }
        return false;
    }

    bool clear() {
        cont.clear();
    }

    int lenght() {
        return (int) cont.size();
    }

    string print(int index) {
        int i = 0;
        for (auto it = cont.begin(); it != cont.end(); ++it, i++) {
            if (index == i) {
                return it->first + ": " + it->second;
            }
        }
    }

    string printValue(int index) {
        int i = 0;
        for (auto it = cont.begin(); it != cont.end(); ++it, i++) {
            if (index == i) {
                return it->second;
            }
        }
    }
    string printIndex(int index) {
        int i = 0;
        for (auto it = cont.begin(); it != cont.end(); ++it, i++) {
            if (index == i) {
                return it->first;
            }
        }
    }
    void printIndex() {
        for (auto it = cont.begin(); it != cont.end(); ++it) {
            cout << it->first << " ";
        }
    }

    string getString() {
        string Properties;
        for (auto it = cont.begin(); it != cont.end(); ++it) {
            Properties += it->first + ":" + it->second + ";\n";
        }
        return Properties;
    }

    VALUE &operator[](const KEY &key) {
        for (typename vector<pair<KEY, VALUE> >::iterator it = cont.begin(); it != cont.end(); ++it)
            if (it->first == key)
                return it->second;
        cont.push_back(make_pair(key, VALUE()));
        return cont.back().second;
    }
};

#endif //LAB7_MAP_H
