#ifndef LAB7_TERMINALROOT_H
#define LAB7_TERMINALROOT_H

#include "Product.h"
#include "Map.h"
#include "Terminal.h"
#include "TerminalPublic.h"
#include <string>

class TerminalRoot: public Terminal {
    friend Map<string, Product> Root_terminal(Map<string, Product> &, TerminalRoot);
    friend bool RootLogin(string, string, TerminalRoot);
private:
    bool removeProduct(Map<std::string, Product>&, std::string);
    bool addProduct(Map<std::string, Product>&);
    bool validation(string);
public:
    TerminalRoot();
    ~TerminalRoot();
    bool removeForPublic(Map<std::string, Product>&, std::string);
};

#endif //LAB7_TERMINALROOT_H
