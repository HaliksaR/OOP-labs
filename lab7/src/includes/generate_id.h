#ifndef LAB7_GENERATE_ID_H
#define LAB7_GENERATE_ID_H

#include <string>
#include "Map.h"
#include "Product.h"
#include <sstream>
#include <iomanip>

std::string generateId(int items, std::string name);

std::string rendering(Map<std::string, Product> Products, std::string id);

std::string mathTab(int);
#endif //LAB7_GENERATE_ID_H
