#ifndef LAB7_PRODUCT_H
#define LAB7_PRODUCT_H

#include "Map.h"
#include <string>

class Product {
    friend Map<string, Product> parsingData(class TerminalPublic&);
    friend Map<string, Product> Root_terminal(Map<string, Product>, string);
    friend class TerminalRoot;
    friend class TerminalPublic;
protected:
    void setQuantity(int);
    void setStatus(int);

private:
    string name;
    int quantity;
    int status;
    string id;
    float price;
    Map<string, string> properties;
    vector<string> propertiesArray;

    void setName(string);
    void setId(string);
    void setPrice(float);
    void setProperties(string, string);
public:
    Product();
    ~Product();

    string getName();
    string getId();
    int getQuantity();
    int getStatus();
    float getPrice();
    string getPropertiesItem(int);
    Map<string, string>getProperties();
};

#endif //LAB7_PRODUCT_H
