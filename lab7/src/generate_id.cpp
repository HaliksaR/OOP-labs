#include "includes/generate_id.h"

std::string generateId(int items, std::string name) {
    for (int i = 0; i < name.length(); i++) {
        name[i] = (name[i]) + rand() % 10;
    }
    std::string done = name + std::to_string(items);
    return done;
}

string rendering(Map<std::string, Product> Products, std::string id) {
    stringstream price;
    price << fixed << setprecision(2) << Products.operator[](id).getPrice();
    return Products.operator[](id).getName() + ":quantity=" + to_string(Products.operator[](id).getQuantity()) +
           +":price=" + price.str() + "{\n"
           + Products.operator[](id).getProperties().getString() + "}\n";
}

std::string mathTab(int size) {
    string str;
    for (int j = (int)(30 - size); j > 0; j--) {
        str += " ";
    }
    return str;
}