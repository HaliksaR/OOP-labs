#include "Terminal.h"
#include <vector>
#include "color.h"
#include <iomanip>
#include <fstream>
#include "Map.h"
#include "../includes/generate_id.h"

Terminal::Terminal() = default;

Terminal::~Terminal() = default;


void Terminal::printMenu() {
    cout << "\33[30;1;41m" << "TERMINAL:" << mathTab(9) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << "{0} Search id by tag" << mathTab(20) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << "{1} Get info by tag or id" << mathTab(25) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << "{2} Buy by tag or id" << mathTab(20) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << "{3} Search by Properties" << mathTab(24) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << mathTab(0) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << mathTab(0) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << "{4} Admin Console" << mathTab(17) << "\33[0m" << endl;
    cout << "\33[0;37;40m" << "{5} Exit" << mathTab(8) << "\33[0m" << endl;
    cout << "Answer: ";
}

void Terminal::print(Map<string, Product> &Products, std::vector<std::string> arrayId) {
    for (int i = 0; i < arrayId.size(); i++) {
        printTitle(Products, arrayId[i]);
        printProperties(Products, arrayId[i]);
    }
}

void Terminal::printId(Map<string, Product> &Products, std::vector<std::string> arrayId) {
    for (int i = 0; i < arrayId.size(); i++) {
        cout << "-----------------------" << endl;
        cout << "Name: " << Products[arrayId[i]].getName() << " | ";
        cout << "Id: " << Products[arrayId[i]].getId() << " | " << endl;
        cout << "-----------------------" << endl;
    }
}

void Terminal::printProperties(Map<string, Product> &Products, string index) {
    cout << "\33[30;1;47m" << "Properties:" << mathTab(11) << "\33[0m" << endl;
    for (int i = 0; i < Products.operator[](index).getProperties().lenght(); i++) {
        cout << "\33[30;0;47m" + Products.operator[](index).getProperties().print(i);
        cout << mathTab((int) Products.operator[](index).getProperties().print(i).length()) << "\33[0m" << endl;
    }
}

void Terminal::printTitle(Map<string, Product> &Products, string index) {
    cout << "\33[0;1;42m" << "Name: " + Products.operator[](index).getName()
         << mathTab((int) ("Name: " + Products.operator[](index).getName()).length()) << "\33[0m" << endl;
    cout << "\33[30;0;47m" << "id: " + index << mathTab((int) ("id: " + index).length()) << "\33[0m" << endl;
    cout << "\33[30;0;47m" << "Quantity: " << Products.operator[](index).getQuantity()
         << mathTab((int) ("Quantity: " + to_string(Products.operator[](index).getQuantity())).length()) << "\33[0m"
         << endl;
    stringstream iterToken;
    iterToken << fixed << setprecision(2) << Products.operator[](index).getPrice() << "$";
    cout << "\33[30;0;47m" << "Price: " << fixed << setprecision(2) << Products.operator[](index).getPrice() << "$"
         << mathTab((int) ("Price: " + iterToken.str()).length()) << "\33[0m" << endl;
    iterToken.clear();
}

//press Ctrl+Alt+M (Refactor | Extract Method).