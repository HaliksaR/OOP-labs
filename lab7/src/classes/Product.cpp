#include "Product.h"

Product::Product() {
    name = "Unknown Product";
    quantity = 0;
    status = 0;
    price = 0.00;

}

Product::~Product() {
    name.clear();
    quantity = 0;
    status = 0;
    price = 0;
    properties.clear();
    propertiesArray.clear();
}


void Product::setName(std::string str) {
    this->name = str;
}

void Product::setId(std::string str) {
    this->id = str;
}

void Product::setQuantity(int num) {
    this->quantity = num;
}

void Product::setStatus(int num) {
    this->status = num;
}

void Product::setPrice(float num) {
    this->price = num;
}

void Product::setProperties(std::string key, std::string val) {
    this->properties.add(key, val);
};

std::string Product::getName() {
    return this->name;
}

std::string Product::getId() {
    return this->id;
}

int Product::getQuantity() {
    return this->quantity;
}

int Product::getStatus() {
    return this->status;
}

float Product::getPrice() {
    return this->price;
}

Map<std::string, std::string> Product::getProperties() {
    return this->properties;
};

string Product::getPropertiesItem(int index) {
    return this->propertiesArray[index];
}