#include "../includes/TerminalPublic.h"
#include "../includes/TerminalRoot.h"
#include <vector>
#include "color.h"
#include <iomanip>

#include "Map.h"
#include "../includes/generate_id.h"

TerminalPublic::TerminalPublic() = default;

TerminalPublic::~TerminalPublic() = default;

vector<string>
TerminalPublic::searchPropertiesProduct(Map<string, Product> Products, string name, string value,
                                        vector<string> arrayId) {
    vector<string> IdFind;
    for (int i = 0; i < arrayId.size(); i++) {
        if (Products.operator[](arrayId[i]).getName() == name && Products.operator[](arrayId[i]).getStatus() != 0) {
            for (int j = 0; j < Products.operator[](arrayId[i]).getProperties().lenght(); j++) {
                if (Products.operator[](arrayId[i]).getProperties().printValue(j) == value) {
                    IdFind.push_back(arrayId[i]);
                }
            }
        }
    }
    if (IdFind.size() > 0) {
        cout << "Found by tag (" + name + " and " + value + "):" << endl;
        for (int i = 0; i < IdFind.size(); ++i) {
            printTitle(Products, IdFind[i]);
            printProperties(Products, IdFind[i]);
        }
        cout << "Find " << IdFind.size() << " products" << endl;
    } else {
        COUTRED << "Found by (" + name + " and " + value + "): NOT FOUND" << ENDLRESET;
    }

    return IdFind;
}

vector<std::string> TerminalPublic::searchProduct(Map<string, Product> Products, string name, vector<string> arrayId) {
    vector<std::string> Result;
    for (int i = 0; i < arrayId.size(); i++) {
        if (Products.operator[](arrayId[i]).getName() == name && Products.operator[](arrayId[i]).getStatus() != 0) {
            Result.push_back(arrayId[i]);
        }
    }
    if (Result.size() > 0) {
        cout << "Found by tag (" + name + "):" << endl;
        for (int i = 0; i < Result.size(); ++i) {
            COUTGREEN << "id: " + Result[i] << ENDLRESET;
        }
    } else {
        COUTRED << "Found by tag (" + name + "): NOT FOUND" << ENDLRESET;
    }
    return Result;
}

Map<string, Product>
TerminalPublic::buyProduct(Map<std::string, Product> &Products, std::string name, int qual,
                           std::vector<std::string>& arrayId) {
    vector<std::string> Result;
    for (int i = 0; i < arrayId.size(); i++) {
        if (Products.operator[](arrayId[i]).getName() == name && Products.operator[](arrayId[i]).getStatus() != 0) {
            Result.push_back(arrayId[i]);
        } else if (name == arrayId[i] && Products.operator[](arrayId[i]).getStatus() != 0) {
            GetInfoProduct(Products, name, arrayId);
            COUTGREEN << "Total Price: " << fixed << setprecision(2)
                      << qual * Products.operator[](arrayId[i]).getPrice() << "$" << ENDLRESET;
            cout << "Confirm?: y or n" << endl;
            char ansv;
            cin >> ansv;
            if (ansv != 'y') {
                cout << "Ok!" << endl;
                return Products;
            }
            if (Products.operator[](arrayId[i]).getQuantity() - qual >= 0) {
                int q = Products.operator[](arrayId[i]).getQuantity() - qual;
                Products.operator[](arrayId[i]).setQuantity(q);
                COUTGREEN << "--------YOU-BUY-----------" << ENDLRESET;
                printTitle(Products, arrayId[i]);
                printProperties(Products, arrayId[i]);
                if (Products.operator[](arrayId[i]).getQuantity() == 0) {
                    Products.operator[](arrayId[i]).setStatus(0);
                    COUTRED << "Product already purchased!" << endl;
                    TerminalRoot RemoveEvents;
                    RemoveEvents.arrayId = arrayId;
                    if (RemoveEvents.removeForPublic(Products, arrayId[i])) {
                        arrayId = RemoveEvents.arrayId;
                    } else {
                        COUTRED << "DELETE ERROR!" << ENDLRESET;
                    }
                    return Products;
                }
                return Products;
            } else {
                COUTRED << "Products less!" << ENDLRESET;
                return Products;
            }
        }
    }
    if (Result.size() > 0) {
        string answ;
        cout << "why buy? (" + name + ")" << endl;
        GetInfoProduct(Products, name, arrayId);
        cout << "Select ID:";
        cin >> answ;
        buyProduct(Products, answ, qual, arrayId);
    } else {
        COUTRED << "Found by tag (" + name + "): NOT FOUND" << ENDLRESET;
    }
    return Products;
}

bool TerminalPublic::GetInfoProduct(Map<std::string, Product> &Products, std::string name,
                                    std::vector<std::string> arrayId) {
    size_t count = 0;

    for (int i = 0; i < arrayId.size(); i++) {
        if (Products.operator[](arrayId[i]).getName() == name && Products.operator[](arrayId[i]).getStatus() != 0) {
            printTitle(Products, arrayId[i]);
            printProperties(Products, arrayId[i]);
            count++;
        } else if (name == arrayId[i] && Products.operator[](arrayId[i]).getStatus() != 0) {
            printTitle(Products, arrayId[i]);
            printProperties(Products, arrayId[i]);
            count++;
        } else if (Products.operator[](arrayId[i]).getStatus() == 0 ||
                   Products.operator[](arrayId[i]).getQuantity() <= 0) {
            cout << "----------NOT HAVE-------------" << endl;
            printTitle(Products, arrayId[i]);
        }
    }
    if (count == 0) {
        COUTRED << "Found by tag (" + name + "): NOT FOUND" << ENDLRESET;
        return false;
    }
    return true;
}