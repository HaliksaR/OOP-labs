#include "TerminalRoot.h"
#include "Terminal.h"
#include <vector>
#include "color.h"
#include <iomanip>
#include <fstream>
#include "Map.h"
#include "../includes/generate_id.h"
#include "../includes/Product.h"

TerminalRoot::TerminalRoot() = default;
TerminalRoot::~TerminalRoot() = default;

bool TerminalRoot::validation(string valid) {
    if (valid == "root&root") {
        return true;
    }
    return false;
}

bool TerminalRoot::addProduct(Map<std::string, Product> &Products) {
    ofstream f("../data/Product.txt", ios::app);
    if (!f.is_open()) {
        return false;
    }
    string str;
    int index = Products.lenght() + 1;
    cout << "Name: ";
    cin >> str;
    string id = generateId(index, str);
    this->arrayId.push_back(id);
    Products.operator[](id).setId(id);
    Products.operator[](id).setName(str);
    cout << "Quantity: ";
    cin >> str;
    Products.operator[](id).setQuantity(stoi(str));
    cout << "Price: ";
    cin >> str;
    Products.operator[](id).setPrice(stof(str));
    cout << "How many properties: ";
    int answ;
    cin >> answ;
    string value;
    for (int i = 0; i < answ; i++) {
        cout << "-----<" + to_string(i) + ">-----" << endl;
        cout << "Properties: ";
        cin >> str;
        cout << "Value:";
        cin >> value;
        Products.operator[](id).setProperties(str, value);
    }
    Products.operator[](id).setStatus(1);
    f << rendering(Products, id);
    return true;
}

bool TerminalRoot::removeProduct(Map<std::string, Product> &Products, string str) {
    for (int i = 0; i < this->arrayId.size(); i++) {
        if (str == this->arrayId[i]) {
            Products.remove(str, Products.operator[](str));
            this->arrayId.erase(this->arrayId.begin() + i);
            ofstream firstfile;
            firstfile.open("../data/Product.txt");
            if (!firstfile.is_open()) {
                return false;
            }
            for (int j = 0; j < this->arrayId.size(); j++) {
                firstfile << rendering(Products, this->arrayId[j]);
            }
            return true;
        }
    }
    return false;
}

bool TerminalRoot::removeForPublic(Map<std::string, Product>&Products, std::string str) {
    return removeProduct(Products, str);
}