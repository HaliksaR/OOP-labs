#ifndef LAB6_VECTOR_H
#define LAB6_VECTOR_H

#include <iostream>
#include "type.h"
#include "Set.h"

using namespace std;

template<typename T>
class Vector : public Set<T> {
protected:
    Vector *next_node_v;
    Vector *prev_node;
public:
    Vector();
    ~Vector();
    int add_node(Vector *, int);
    int delete_node(int);
    void show_vector();
    T lookup(int);

    Vector &operator[](int index) {
        while (index < 0 || index >= this->size) {
            if (index < 0) {
                index = this->size + index;
            } else {
                index = index - this->size;
            }
        }
        Vector *every_next_pointer = this;
        while (every_next_pointer->index != index) {
            every_next_pointer = every_next_pointer->next_node_v;
        }
        return *every_next_pointer;
    }
};


template<typename T>
Vector<T>::Vector() {
    next_node_v = NULL;
    prev_node = NULL;
}

template<typename T>
Vector<T>::~Vector() {
    next_node_v = NULL;
    prev_node = NULL;
}

template<typename T>
void Vector<T>::show_vector() {
    cout << "\nVector:" << endl;
    Vector *every_next_pointer = this;

    for (int i = 0; (i < this->size) && (every_next_pointer != NULL); i++) {
        cout << "\t\t| Index: " << every_next_pointer->index << " |";
        if (every_next_pointer->next_node_v == NULL) {
            cout << "| Next node: NULL |";
        } else {
            cout << "| Next node: " << every_next_pointer->next_node_v->index << " |";
        }
        if (every_next_pointer->prev_node == NULL) {
            cout << "| Prev node: NULL |";
        } else {
            cout << "| Prev node: " << every_next_pointer->prev_node->index << " |";
        }
        if (every_next_pointer->value == 99999) {
            cout << "| Value:\033[35m " << every_next_pointer->value << "\033[0m |";
        } else {
            cout << "| Value: " << every_next_pointer->value << " |";
        }
        cout  << "| flag: " << every_next_pointer->flag << " |"
             << "| Size: " << every_next_pointer->size << " |" << endl;
        every_next_pointer = every_next_pointer->next_node_v;
    }
}

template<typename T>
int Vector<T>::add_node(Vector *vectorObj, int index) {
    try {
        Vector *every_next_pointer = this;
        if (index > this->size) index = this->size;
        if (index < 0) index = 0;
        if (index >= 0) {
            if (index == this->size) {
                cout << "Add element in end..." << endl;
                while (every_next_pointer->next_node_v != NULL) {
                    every_next_pointer = every_next_pointer->next_node_v;
                }
                every_next_pointer->next_node_v = vectorObj;
                vectorObj->index = this->size;
                vectorObj->flag = 1;
                vectorObj->next_node_v = NULL;
                vectorObj->prev_node = every_next_pointer;
                this->size += 1;
                every_next_pointer = this;
                while (every_next_pointer != NULL) {
                    every_next_pointer->size = this->size;
                    every_next_pointer = every_next_pointer->next_node_v;
                }
                throw 1;
            } else if (index == 0) {
                if (this->flag == 0) {
                    cout << "Initialization..." << endl;
                    this->next_node_v = NULL;
                    this->prev_node = NULL;
                    this->value = vectorObj->value;
                    this->index = 0;
                    this->size = 1;
                    this->flag = 1;
                    throw 1;
                } else {
                    cout << "Add element in start..." << endl;

                    Vector buff, *temp = &buff;
                    temp->value = this->value;
                    temp->index = 0;
                    temp->flag = 1;
                    temp->size = this->size;
                    temp->next_node_v = this->next_node_v;
                    temp->prev_node = NULL;

                    this->value = vectorObj->value;
                    this->index = 0;
                    this->flag = 1;
                    this->size += 1;
                    this->next_node_v = vectorObj;
                    this->prev_node = NULL;

                    vectorObj->value = temp->value;
                    vectorObj->index = temp->index;
                    vectorObj->flag = temp->flag;
                    vectorObj->size = temp->size;
                    vectorObj->next_node_v = temp->next_node_v;
                    vectorObj->prev_node = this;
                    if (vectorObj->next_node_v != NULL) {
                        vectorObj->next_node_v->prev_node = vectorObj;
                    }

                    every_next_pointer = this->next_node_v;
                    while (every_next_pointer != NULL) {
                        every_next_pointer->index += 1;
                        every_next_pointer->size = this->size;
                        every_next_pointer = every_next_pointer->next_node_v;
                    }
                    throw 1;
                }
            } else {
                cout << "\nFree position..." << endl;

                every_next_pointer = this;
                while (every_next_pointer->next_node_v->index != index) {
                    every_next_pointer = every_next_pointer->next_node_v;
                }
                vectorObj->next_node_v = every_next_pointer->next_node_v;
                vectorObj->prev_node = every_next_pointer;
                every_next_pointer->next_node_v->prev_node = vectorObj;
                every_next_pointer->next_node_v = vectorObj;
                vectorObj->index = index;
                vectorObj->flag = 1;
                this->size += 1;
                every_next_pointer = vectorObj->next_node_v;
                while (every_next_pointer != NULL) {
                    every_next_pointer->index += 1;
                    every_next_pointer = every_next_pointer->next_node_v;
                }
                every_next_pointer = this;
                while (every_next_pointer != NULL) {
                    every_next_pointer->size = this->size;
                    every_next_pointer = every_next_pointer->next_node_v;
                }
                throw 1;
            }
        } else {
            throw 0;
        }
    }

    catch (int a) {
        return a;
    }
}

template<typename T>
int Vector<T>::delete_node(int index) {
    try {
        if (index >= this->size) index = this->size - 1;
        if (index < 0) index = 0;
        if (this->flag == 1) {
            if (index >= 0 && index <= this->size - 1) {
                if (index == 0) {
                    cout << "Delete head..." << endl;
                    if (this->next_node_v != NULL) {
                        Vector *every_next_pointer;

                        this->value = this->next_node_v->value;
                        this->index = 0;
                        this->flag = 1;
                        this->size -= 1;
                        if (this->size == 0) {
                            this->flag = 0;
                        }

                        this->next_node_v = this->next_node_v->next_node_v;
                        this->prev_node = NULL;
                        if (this->next_node_v != NULL) this->next_node_v->prev_node = this;
                        every_next_pointer = this->next_node_v;

                        while (every_next_pointer != NULL) {
                            every_next_pointer->size = this->size;
                            every_next_pointer->index -= 1;
                            every_next_pointer = every_next_pointer->next_node_v;
                        }
                        throw 1;
                    } else {
                        // в списке один элемент
                        this->value = 1;
                        this->index = 0;
                        this->flag = 0;
                        this->size = -1;
                        this->next_node_v = NULL;
                        this->prev_node = NULL;
                        throw 1;
                    }
                } else {
                    // удаляется не первый элемент
                    Vector *every_next_pointer = this;
                    this->size -= 1;

                    while (every_next_pointer->next_node_v->index != index) {
                        every_next_pointer = every_next_pointer->next_node_v;
                    }
                    every_next_pointer->next_node_v = every_next_pointer->next_node_v->next_node_v;
                    if (every_next_pointer->next_node_v != NULL) {
                        every_next_pointer->next_node_v->prev_node = every_next_pointer;
                    }
                    every_next_pointer = every_next_pointer->next_node_v;
                    while (every_next_pointer != NULL) {
                        every_next_pointer->index -= 1;
                        every_next_pointer = every_next_pointer->next_node_v;
                    }
                    every_next_pointer = this;
                    while (every_next_pointer != NULL) {
                        every_next_pointer->size = this->size;
                        every_next_pointer = every_next_pointer->next_node_v;
                    }
                    throw 1;
                }
            } else {
                throw 0;
            }
        } else {
            throw 0;
        }
    }

    catch (int a) {
        return a;
    }
}

template<typename T>
T Vector<T>::lookup(int index) {
    if (this->flag == 1 && index >= 0 && index < this->size) {
        Vector *every_next_pointer = this;
        while (every_next_pointer->index != index) {
            every_next_pointer = every_next_pointer->next_node_v;
        }
        return every_next_pointer->value;
    } else {
        return -1;
    }
}


#endif //LAB6_VECTOR_H
