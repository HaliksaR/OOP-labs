#ifndef LAB6_SET_H
#define LAB6_SET_H

#include <iostream>
#include "type.h"

using namespace std;

template<typename T>
class Set {
protected:
    T value;
    int index;
    int flag;
    int size;
    Set *next_node;
public:
    Set();
    ~Set();
    int add_node(Set *, int);
    void put_value(T);
    int delete_node(int);
    void show_set();
    T lookup(int);
};



template<typename T>
Set<T>::Set() {
    value = 1;
    size = -1;
    next_node = NULL;
    index = 0;
    flag = 0;
}

template<typename T>
Set<T>::~Set() {
    value = -1;
    size = 0;
    next_node = NULL;
    index = -1;
    flag = 0;
}

template<typename T>
void Set<T>::put_value(T a) {
    this->value = a;
}

template<typename T>
void Set<T>::show_set() {
    cout << "\nSet:" << endl;
    Set *everyNextPointer = this;

    for (int i = 0; (i < this->size) && (everyNextPointer != NULL); i++) {
        cout << "\t\t| Index: " << everyNextPointer->index << " |";
        if (everyNextPointer->next_node == NULL) {
            cout << "| Next node: NULL |";
        } else {
            cout << "| Next node: " << everyNextPointer->next_node->index << " |";
        }
        cout << "| Value: " << everyNextPointer->value << " |"
             << "| flag: " << everyNextPointer->flag << " |"
             << "| Size: " << everyNextPointer->size << " |" << endl;
        everyNextPointer = everyNextPointer->next_node;
    }
}

template<typename T>
int Set<T>::add_node(Set *setObj, int index) {
    try {
        if (index > this->size) index = this->size;
        if (index < 0) index = 0;
        Set *everyNextPointer = this;

        if (index >= 0) {
            if (index == this->size) {
                cout << "Add element in end..." << endl;
                while (everyNextPointer->next_node != NULL) {
                    everyNextPointer = everyNextPointer->next_node;
                }
                everyNextPointer->next_node = setObj;
                setObj->index = everyNextPointer->index + 1;
                setObj->size = everyNextPointer->size + 1;
                setObj->flag = 1;

                this->flag = 1;
                this->size += 1;

                everyNextPointer = this->next_node;
                while (everyNextPointer != NULL) {
                    everyNextPointer->size = this->size;
                    everyNextPointer = everyNextPointer->next_node;
                }
                throw 1;
            } else if (index == 0) {
                if (this->flag == 0) {
                    cout << "Initialization..." << endl;
                    this->value = setObj->value;
                    this->index = 0;
                    this->flag = 1;
                    this->size = 1;
                    this->next_node = NULL;
                    throw 1;
                } else {
                    cout << "Add element in start..." << endl;
                    Set buff;
                    Set *temp = &buff, *everyNextPointer;

                    temp->value = this->value;
                    temp->index = 0;
                    temp->flag = 1;
                    temp->size = this->size;
                    temp->next_node = this->next_node;

                    this->value = setObj->value;
                    this->index = 0;
                    this->flag = 1;
                    this->size = temp->size;
                    this->next_node = setObj;

                    setObj->value = temp->value;
                    setObj->index = temp->index;
                    setObj->flag = 1;
                    setObj->size = temp->size;
                    setObj->next_node = temp->next_node;

                    this->size += 1;
                    everyNextPointer = this->next_node;
                    while (everyNextPointer != NULL) {
                        everyNextPointer->size = this->size;
                        everyNextPointer->index += 1;
                        everyNextPointer = everyNextPointer->next_node;
                    }
                    throw 1;
                }
            } else {
                cout << "Add element in free position..." << endl;
                Set *everyNextPointer;

                everyNextPointer = this;
                while (everyNextPointer->next_node->index != index) {
                    everyNextPointer = everyNextPointer->next_node;
                }
                setObj->index = index;
                setObj->flag = 1;
                setObj->next_node = everyNextPointer->next_node;

                everyNextPointer->next_node = setObj;
                everyNextPointer = setObj->next_node;
                while (everyNextPointer != NULL) {
                    everyNextPointer->index += 1;
                    everyNextPointer = everyNextPointer->next_node;
                }

                this->size += 1;
                everyNextPointer = this->next_node;
                while (everyNextPointer != NULL) {
                    everyNextPointer->size = this->size;
                    everyNextPointer = everyNextPointer->next_node;
                }
                throw 1;
            }
        } else {
            throw 0;
        }
    }

    catch (int a) {
        return a;
    }
}

template<typename T>
int Set<T>::delete_node(int index) {
    try {
        if (index > this->size) index = this->size - 1;
        if (index < 0) index = 0;
        if (this->flag == 1) {
            if (index >= 0 && index <= this->size - 1) {
                if (index == 0) {
                    cout << "Delete head..." << endl;
                    if (this->next_node != NULL) {
                        Set *everyNextPointer;

                        this->value = next_node->value;
                        this->index = 0;
                        this->size -= 1;
                        this->flag = 1;
                        if (size == 0) {
                            flag = 0;
                        }
                        this->next_node = this->next_node->next_node;
                        everyNextPointer = this->next_node;
                        while (everyNextPointer != NULL) {
                            everyNextPointer->size = this->size;
                            everyNextPointer->index -= 1;
                            everyNextPointer = everyNextPointer->next_node;
                        }
                        throw 1;
                    } else {
                        // в списке один элемент
                        this->value = 1;
                        this->index = 0;
                        this->size = -1;
                        this->flag = 0;
                        this->next_node = NULL;
                        throw 1;
                    }
                } else {
                    // удаляется не первый элемент
                    Set *everyNextPointer = this;
                    this->size -= 1;
                    while (everyNextPointer->next_node->index != index) {
                        everyNextPointer = everyNextPointer->next_node;
                    }
                    everyNextPointer->next_node = everyNextPointer->next_node->next_node;
                    everyNextPointer = everyNextPointer->next_node;
                    while (everyNextPointer != NULL) {
                        everyNextPointer->index -= 1;
                        everyNextPointer = everyNextPointer->next_node;
                    }
                    everyNextPointer = this;
                    while (everyNextPointer != NULL) {
                        everyNextPointer->size = this->size;
                        everyNextPointer = everyNextPointer->next_node;
                    }
                    throw 1;
                }
            } else {
                throw 0;
            }
        } else {
            throw 0;
        }
    }

    catch (int a) {
        return a;
    }
}

template<typename T>
T Set<T>::lookup(int index) {
    if (flag == 1 && index >= 0 && index < this->size) {
        Set *everyNextPointer = this;
        while (everyNextPointer->index != index) {
            everyNextPointer = everyNextPointer->next_node;
        }
        return everyNextPointer->value;
    } else {
        return -1;
    }
}

#endif //LAB6_SET_H
