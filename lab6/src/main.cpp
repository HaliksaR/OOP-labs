#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <ctime>

#include "Set.h"
#include "Vector.h"
#include "type.h"
using namespace std;


int setFunc(TYPE *array);
int vectorFunc(TYPE *array);
void printArray(TYPE *array);

int main() {
    srand(static_cast<unsigned int>(time(NULL)));
    auto *array = new TYPE[MAX_ITEMS];
    for (size_t i = 0; i < MAX_ITEMS; i++) {
        array[i] = fmod(static_cast <TYPE> (rand()), static_cast <TYPE> (MAX_RAND));
    }

    int value;
    cout << endl << "{3}Exit {1}Set or {2}Vector: ";
    cin >> value;
    switch (value) {
        case 1:
            printArray(array);
            setFunc(array);
            break;
        case 2:
            printArray(array);
            vectorFunc(array);
            break;
        case 3:
            return 0;
        default:
            return 0;
    }
    return 0;
}

void printArray(TYPE *array) {
    cout << "Array:" << endl;
    for (size_t i = 0; i < MAX_ITEMS; i++) {
        cout << " {"<< i << "} = "<< array[i] << endl;
    }
}

int setFunc(TYPE *array) {
    int index = 0;
    TYPE value = 0;

    Set<TYPE> *vector = new Set<TYPE>[MAX_ITEMS], head;

    vector[0].put_value(array[0]);
    head.add_node(&vector[0], 0);
    head.show_set();

    for (size_t j = 1; j < MAX_ITEMS; j++) {
        printArray(array);
        vector[j].put_value(array[j]);
        cout <<"Put index for adding elements: ";
        cin >> index;
        if (head.add_node(&vector[j], index)) {
            head.show_set();
        } else {
            return 0;
        }
    }

    bool done = false;
    while (!done) {
        cout << "lookup value: ";
        cin >> index;
        value = head.lookup(index);
        if (value > 0) {
            cout << "Find: "<< value << endl;
            done = true;
        } else {
            cout << "Incorrect index!" << endl;
        }
    }
    int del = 0;
    for (size_t j = 0; j < MAX_ITEMS; j++) {
        done = false;
        while (!done) {
            cout << "Delete index: ";
            cin >> index;
            if (MAX_ITEMS - del > index) {
                if (!head.delete_node(index)) {
                    return 0;
                } else {
                    done = true;
                    head.show_set();
                }
            } else {
                cout << "Incorrect index!" << endl;
            }
        }
        del++;
    }
    return 0;
}

int vectorFunc(TYPE *array) {
    int index = 0;
    TYPE value = 0;

    Vector<TYPE> head, *vector = new Vector<TYPE>[MAX_ITEMS];

    vector[MAX_ITEMS - 1].put_value(array[MAX_ITEMS - 1]);
    head.add_node(&vector[MAX_ITEMS - 1], 0);
    head.show_vector();

    for (int j = MAX_ITEMS - 2; j >= 0; j--) {
        printArray(array);
        vector[j].put_value(array[j]);
        cout << "Please, put index for adding:";
        cin >> index;
        if (head.add_node(&vector[j], index)) {
            head.show_vector();
            cout << "Left inserts: " << j << endl;
        }
    }

    bool done = false;
    while (!done) {
        cout << "Lookup value: ";
        cin >> index;
        value = head.lookup(index);
        if (value > 0) {
            cout << "Value: " << value << endl;
            done = true;
        } else {
            cout << "Incorrect index!" << endl;
        }
    }

    done = false;
    while (!done) {
        cout << "Input the index:";
        cin >> index;
        if (MAX_ITEMS > index) {
            Vector<TYPE> &vRef = head[index];
            vRef.put_value(99999);
            head.show_vector();
            done = true;
        } else {
            cout << "Incorrect index!" << endl;
        }
    }

    int del = 0;
    for (size_t j = 0; j < MAX_ITEMS; j++) {
        done = false;
        while (!done) {
            cout << "Delete items: ";
            cin >> index;
            if (MAX_ITEMS - del > index) {
                if (!head.delete_node(index)) {
                    return 0;
                } else {
                    done = true;
                    head.show_vector();
                }
            } else {
                cout << "Incorrect index!" << endl;
            }
        }
        del++;
    }
    return  0;
}