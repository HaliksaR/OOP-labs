#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <cstdio>

using namespace std;

class Menu {
    friend int main();

private:
    ifstream ifs;    // outside file stream
    ofstream ofs;    // inside file stream
    string name;    // name of open file
    string buf;        // buffer for all manipulation
    string text;    // all text
public:
    Menu();        // constructor
    ~Menu();    // destructor

    void open_file(string a);    // to open file
    void add_string(string a);    // add string to
    void show_text();            // show modifited text
    int search_word(string a);    // how many repetitions?
    int delete_word(string a);    // delete all word a in the text (points)
    void clear_text();            // to clear all terminal text
    void save(string a);        // to save text in the file <a>
};

typedef class Menu CMenu;