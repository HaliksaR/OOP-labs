#include "Menu.h"

int main() {
    Menu m; // object was created by constructor
    // great opening
    string file_name;
    cout << "Input name of file: ";
    getline(cin, file_name);
    m.open_file(file_name);
    m.show_text();
    // you can add a word
    string word;
    cout << "Input the string: ";
    getline(cin, word);
    m.add_string(word);
    m.show_text();
    // you can search word in the text
    string search;
    cout << "We can search word... ";
    cin >> search;
    int q = 0;
    q = m.search_word(search);
    cout << endl << "Your text have " << q << " word(s) <" << search << ">" << endl;
    m.show_text();
    // you can delete all repeteitions
    string del;
    cout << "You can delete word... ";
    cin >> del;
    int d = 0;
    d = m.delete_word(del);
    cout << endl << "We delete " << d << " word(s) <" << del << ">" << endl;
    m.show_text();
    // ew can save you changes in the file
    string save_name;
    cout << "You can save text in the file... ";
    cin >> save_name;
    m.save(save_name);
    // oops, it was a misstake...
    m.clear_text();
    m.show_text();
    return 0;
}