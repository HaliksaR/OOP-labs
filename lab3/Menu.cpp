#include "Menu.h"

Menu::Menu() {
    string name = "\0";
    string buf = "\0";
    string text = "\0";
}

Menu::~Menu() {
    ofs.close();
    ifs.close();
    delete &text;
    delete &name;
    delete &buf;
}

void Menu::open_file(string a) {
    name = a;
    ifs.open(name);

    if (!name.size()) {
        cerr << "Error: file not found!" << endl;
    } else {
        while (getline(ifs, buf)) {
            text += "\n";
            text += buf;
        }
    }
}

void Menu::add_string(string a) {
    buf = a;
    text += " ";
    text += buf;
    text += "\n";
}

void Menu::show_text() {
    cout << endl << "Text: " << endl << text << endl;
}

int Menu::search_word(string a) {
    int score = 0, j = 0, p = 0, i = 0, flag = 1;
    buf = a;

    int word_size = buf.size();
    int text_size = text.size();
    cout << "lenght of text: " << text_size << endl;
    cout << "lenght of word: " << word_size << endl;

    for (i = 0; i < text_size - word_size; i++) {
        if (text[i] == buf[0]) {
            flag = 1;
            for (j = 0, p = i; (j < word_size) && (flag == 1); j++, p++) {
                if (text[p] != buf[j]) {
                    flag = 0;
                }
            }
            if (flag == 1) {
                score++;
            }
        }
    }
    return score;
}

int Menu::delete_word(string a) {
    buf = a;
    int *score = new int[1];
    *score = 0;

    cout << endl << "Delete: " << buf << endl;
    int j = 0, p = 0, i = 0, h = 0, flag = 1;

    int word_size = buf.size();
    int text_size = text.size();
    cout << "lenght of text: " << text_size << endl;
    cout << "lenght of word: " << word_size << endl;

    for (i = 0; i < text_size - word_size; i++) {
        if (text[i] == buf[0]) {
            flag = 1;
            for (j = 0, p = i; (j < word_size) && (flag == 1); j++, p++) {
                if (text[p] != buf[j]) {
                    flag = 0;
                }
            }
            if (flag == 1) {
                for (h = i; h < i + word_size; h++) {
                    text[h] = '.';
                }
                *(score) += 1;
            }
        }
    }
    return *score;
}

void Menu::clear_text() {
    text = "\0";
    cout << endl << "Clear text... " << endl;
}

void Menu::save(string a) {
    cout << "a: " << a << endl;
    cout << "T: " << endl << text;
    ofs.open(a);
    if (ofs.is_open() == 1) {
        cout << "save all good" << endl;
    } else {
        cerr << "save all bad" << endl;
    }
    ofs << text;
    return;
}