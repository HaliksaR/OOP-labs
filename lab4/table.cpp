#include "table.h"
#include "color.h"

Table::Table() {
    columns = 0;
    lines = 0;
    string name = "\0";
    table_buff = new string *[lines];
    for (size_t i = 0; i < lines; i++) {
        table_buff[i] = new string[columns];
    }
}

Table::~Table() {
    ifs.close();
    for (size_t i = 0; i < lines; i++) {
        delete[]table_buff[i];
    }
}


void Table::math_cl() {
    string buff;
    while (getline(ifs, buff, '|') && !ifs.eof()) {
        if (buff != "\n") {
            if (buff[0] != 0) {
                columns++;
            }
        }
        if (buff == "\n") {
            lines++;
            columns = 0;
        }
    }
    ifs.close();
}

int Table::open_file(string a) {
    name = a;
    try {
        cout << "File open..." << endl;
        ifs.open(name);
        if (!ifs) {
            throw "Error: file not found!";
        }
        cout << GREEN << "Done!" << RESET << endl;
    }
    catch (const char *ex) {
        cerr << RED << ex << RESET << endl;
        return -1;
    }
    return 0;
}

int Table::create_table_buff() {
    table_buff = new string *[lines + 1];
    for (size_t i = 0; i < lines + 1; i++) {
        table_buff[i] = new string[columns + 1];
    }
    return 0;
}

int Table::write_table(string a) {
    size_t i = 0, j = 0;
    string buff;
    open_file(a);
    while (getline(ifs, buff, '|')) {
        if (buff != "\n") {
            if (i == columns) i = 0;
            if (j == lines + 1) break;
            if (buff[0] != 0) {
                table_buff[j][i] = buff;
                i++;
            }
        }
        if (buff == "\n") j++;
    }
    cout << endl;
    return 0;
}

void Table::show_table() {
    for (int i = 0; i < lines + 1; i++) {
        for (int j = 0; j < columns; j++) {
            cout << table_buff[i][j] << '\t' << '\t';
        }
        cout << endl;
    }
}

