#ifndef LAB1_TABLE_H
#define LAB1_TABLE_H
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <cstdio>

using namespace std;

class Table {

private:
    ifstream ifs;
    string name;
    int columns;
    int lines;
    string **table_buff;
public:
    Table();
    ~Table();

    int open_file(string a);
    void math_cl();
    int create_table_buff();
    int write_table(string a);
    void show_table();
};


#endif