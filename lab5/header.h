#include <iostream>
#include <cstdio>
#include <cstdlib>

#define n 10

using namespace std;

class Set {
    friend int main();

protected:
    // доступ есть у наследствующего класса
    // доступ есть у дружественной функции
    // доступ есть у самого класса Set
    int *list;
private:
    // доступ есть у дружественной функции
    // доступ есть у самого класса Set
    int size;
public:
    Set();    // constructor
    ~Set();    // destructor
    int add_node(int);    // to add node in the list
};

class Queue : public Set {
    friend int main();

protected:
    int head;    // head of queue
    int tail;    // end of queue
public:
    Queue();    // constructor
    ~Queue();    // destructor
    int input_head(int);    // to add elem in the head of list
    int output_tail();        // to get last elem of queue
    void print();            // presentation queue
};

class Stek : public Set {
    friend int main();

protected:
    int top;    // top of stek
    int size;    // size of stek
public:
    Stek();    // constructor
    ~Stek();// destructor
    int input_top(int);        // to add elem on the stek
    int output_top();        // to get top elem
    void print();            // presentation stek
};
