#include "header.h"


Set::Set() {
    list = new int[n];
    for (int i = 0; i < n; i++) {
        list[i] = 0;
    }
    size = 0;
    cout << "\t*-Set: constructor-*" << endl;
}

Set::~Set() {
    delete list;
    size = 0;
    cout << "\t*-Set: destructor-*" << endl;
}

int Set::add_node(int a) {
    if (size < 10) {
        list[size] = a;
        size++;
        return 1;    // all good
    } else {
        return 0;    // end of list
    }
}


Queue::Queue() {
    head = 0;
    tail = 0;
    cout << "\t\t*-Queue: constructor-*" << endl;
}

Queue::~Queue() {
    head = 0;
    tail = 0;
    cout << "\t\t*-Queue: destructor-*" << endl;
}

int Queue::input_head(int a) {
    int new_size = tail + 1;
    if (tail < n - 1) {
        for (int i = new_size; i > 0; i--) {
            list[i] = list[i - 1];
        }
        list[head] = a;
        if (list[new_size] != 0) {
            tail = new_size;
        }
        return 1;
    } else {
        return 0;
    }
}

int Queue::output_tail() {
    int a = list[tail];
    list[tail] = 0;

    if (tail > 0) {
        tail--;
    }
    return a;
}

void Queue::print() {
    cout << "\nQueue:\n\n";
    for (int i = 0; i < n; i++) {
        if (i == head) {
            cout << " \033[32m" << list[i] << "\033[0m";
        } else if (i == tail) {
            cout << " \033[31m" << list[i] << "\033[0m";
        } else {
            cout << " " << list[i];
        }
    }
    cout << endl << endl;
}


Stek::Stek() {
    top = 0;
    size = 0;
    cout << "\t\t*-Stek: constructor-*" << endl;
}

Stek::~Stek() {
    top = 0;
    size = 0;
    cout << "\t\t*-Stek: destructor-*" << endl;
}

int Stek::input_top(int a) {
    if (size < n) {
        size++;
        for (int i = size; i > 0; i--) {
            list[i] = list[i - 1];
        }
        list[top] = a;
        return 1;
    } else {
        return 0;
    }
}

int Stek::output_top() {
    int a = list[top];
    if (size > 0) {
        size--;
        for (int i = 0; i < n - 1; i++) {
            list[i] = list[i + 1];
        }
        list[n - 1] = 0;
        return a;
    } else {
        return 0;
    }
}

void Stek::print() {
    cout << "\nStek:\n\n";
    for (int i = 0; i < n; i++) {
        if (i == top) {
            cout << " \033[32m" << list[i] << "\033[0m";
        } else {
            cout << " " << list[i];
        }
    }
    cout << endl << endl;
}
