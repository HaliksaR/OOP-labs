#include "header.h"

int main() {
    int flag = 1, choice = 0, add = 0;
    Queue q;
    Stek s;
    cout << "\nMenu:\n"
         << "\tAdd = (1)\n"
         << "\tGet = (2)\n"
         << "\tExit = (3)\n" << endl;

    q.print();
    s.print();
    while (flag) {
        cout << "Input your choice: " << endl;
        cin >> choice;
        switch (choice) {
            case 1:
                cout << "Input elem: " << endl;
                cin >> add;
                if (!q.input_head(add)) {
                    cout << "Queue is full!" << endl;
                }
                if (!s.input_top(add)) {
                    cout << "Stek is full!" << endl;
                }
                q.print();
                s.print();
                break;
            case 2:
                cout << "Last elem of Queue: " << q.output_tail() << endl;
                q.print();
                cout << "Top elem of Stek: " << s.output_top() << endl;
                s.print();
                break;
            case 3:
                flag = 0;
                break;
            default:
                return 0;
        }
    }

    return 0;
}
